#include <iostream>
#include <ostream>
#include <cmath>
#include <cassert>
#include <complex>
#include "Vector2D.hpp"
#include "Vector3D.hpp"
/*
 *	William Doyle
 *	December 28th 2020
 *
 *	Notable resourses:
 *	https://www.mathsisfun.com/algebra/vectors-cross-product.html
 *	https://mathinsight.org/definition/magnitude_vector
 *	https://www.mathsisfun.com/algebra/vectors-dot-product.html
 *	https://www.omnicalculator.com/math/angle-between-two-vectors
 *	
 *	TO IMPLEMENT:
 *		Vector addition
 *		Vector subtraction
 *		scaler multiplication
 *		dot product
 *		cross product
 *		equivelence operator	
 *	
 * */

#define TYPE std::complex<float> 
//#define TYPE float
template class Vector2D<TYPE>;
template class Vector3D<TYPE>;

int main () {
	
	Vector2D<TYPE>* v2d = new Vector2D<TYPE>(TYPE(3.7), TYPE(9.4));
	std::cout << *v2d << "\n";
	std::cout << "Magnitude is "<< v2d->Magnitude() << "\n";

	Vector2D<TYPE>* second2dv = new Vector2D<TYPE>(1.1, 2.2);
	std::cout << "addition (2D) " << *v2d + *second2dv << "\n\n";
	std::cout << "subtraction (2D) " << *v2d - *second2dv << "\n\n";
	
	Vector3D<TYPE>* v3d = new Vector3D<TYPE>(-1.2, 2.3, 7.9);
	Vector3D<TYPE>* second3dv = new Vector3D<TYPE>(1.2, 2.3, 7.1);

	std::cout << *v3d << "\n";
	std::cout << "Magnitude is " << v3d->Magnitude() << "\n";
	std::cout << "addition (3D) " << *v3d + *second3dv << "\n\n";
	std::cout << "subtraction (3D) " << *v3d - *second3dv << "\n\n";

	std::cout << "addition (3D + 2D) " << *v3d + *second2dv << "\n"; 
	std::cout << "addition (2D + 3D) " <<  *second2dv + *v3d << "\n"; 

	std::cout << "3D vec from 2D vec\t" << Vector3D<TYPE>(*v2d) << "\n";
	std::cout << "again but different\t" << Vector3D<TYPE>(*v2d, 7.77) << "\n";
	
	Vector3D<TYPE>* A = new Vector3D<TYPE>(2,3,4);
	Vector3D<TYPE>* B = new Vector3D<TYPE>(5,6,7);

	std::cout << "Cross product of A and B is " << Vector3D<TYPE>::CrossProduct(*A, *B) << "\n";
	std::cout << "Dot product of A and B is " << Vector3D<TYPE>::DotProduct(*A, *B) << "\n\n";

	// test dot product with numbers from mathsisfun.com (see file level comment) because the expected result for these values is known (it's 122)
	Vector3D<TYPE>* C = new Vector3D<TYPE>(4,8,10);
	Vector3D<TYPE>* D = new Vector3D<TYPE>(9,2,7);
	std::cout << "Dot product of C and D is " << Vector3D<TYPE>::DotProduct(*C, *D) << "\n";

	Vector2D<TYPE>* E = new Vector2D<TYPE>(1.12, 1.0);
	Vector2D<TYPE>* F = new Vector2D<TYPE>(0.1, 0.1);

	std::cout << "Angle between E and F in radians is " <<  Vector2D<TYPE>::AngleBetween_radians(*E, *F) << "\n";
	std::cout << "In degrees this is " << Vector2D<TYPE>::AngleBetween_degrees(*E, *F) << "\n\n";
	
	Vector2D<TYPE>* G = new Vector3D<TYPE>(0.3, 1.0, 0.3);
	Vector2D<TYPE>* H = new Vector3D<TYPE>(0.1, 0.1, 1.1);

	std::cout << "Angle between E and F in radians is " <<  Vector3D<TYPE>::AngleBetween_radians(*G, *H) << "\n";
	std::cout << "In degrees this is " << Vector3D<TYPE>::AngleBetween_degrees(*G, *H) << "\n";

	Vector2D<TYPE>* I = new Vector2D<TYPE>(2.0, 3.0); 
	std::cout << "Scaler multiplication of Vector2D. Original " << *I << ". Multiplied by 2.0 gives " << *I * 2.0 << ".\n";
	std::cout << "Scaler multiplication of Vector2D. Original " << *I << ". Multiplied by 2.0 gives " << 2.0 * *I  << ".\n";


	assert ( *I * 3.0 == 3.0 * *I);


	std::cout << "Scaler multiplication of Vector3D. Original " << *D << ". Multiplied by 2.0 gives " << *D * 2.0 << ".\n";
	std::cout << "Scaler multiplication of Vector3D. Original " << *D << ". Multiplied by 2.0 gives " <<  2.0  * *D << ".\n";

	assert ( *D * 3.0 == 3.0 * *D);

	std::cout << "3D vector - 2D vector " << *C - *E << "\n";
	std::cout << "2D vector - 3D vector " << *E - *C << "\n";
	
	std::cout << "3D vector + 2D vector " << *C + *E << "\n";
	std::cout << "2D vector + 3D vector " << *E + *C << "\n";

	std::cout << "The angle between C and D is " << Vector3D<TYPE>::AngleBetween_radians(*C, *D) << " radians\n";  
	std::cout << "or in degrees " << Vector3D<TYPE>::AngleBetween_radians(*C, *D) << " degrees\n";  
	
	std::cout << "The angle between C and E is " << Vector3D<TYPE>::AngleBetween_radians(*C, *E) << " radians\n";

//	std::cout << "e is " << std::numbers::e << "\n";	

	delete v2d;
	delete second2dv;
	delete v3d;
	delete second3dv;
   	delete A;	
   	delete B;
   	delete C;	
   	delete D;
   	delete E;	
   	delete F;
   	delete G;	
   	delete H;
   	delete I;

	return 1;
}
