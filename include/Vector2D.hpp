#pragma once
#include <cmath>
#include <ostream>
#include <iostream>
/*
 *	Vector2d.hpp
 *	William Doyle
 *	January 1st 2021
 *
 * */

#define SINGLE_RADIAN_IN_DEGREES 57.2958

/*	William Doyle
 *	class Vector2D
 *	December 28th 2020 
 *	A vector with an 'x' component and a 'y' component
 *
 *	Modified to use references - January 27th 2021
 *
 * */
template<class T> 
class Vector2D {
	public:
		T x_comp, y_comp;						// components

		// construct from two doubles
		Vector2D(const T& x_comp, const T& y_comp){
			this->x_comp = x_comp;
			this->y_comp = y_comp;	
		}			

		// destructor	
		virtual ~Vector2D(){  }	

		// get the magnitude of this vector 
		virtual T Magnitude() const {
			return sqrt( pow(this->x_comp, 2) + pow(this->y_comp, 2) );
		}

		// addition
		friend Vector2D<T> operator + (const Vector2D<T>& rhs, const Vector2D<T>& lhs){
			return Vector2D(rhs.x_comp + lhs.x_comp, rhs.y_comp + lhs.y_comp);
		}

		// subtraction
		friend Vector2D<T> operator - (const Vector2D<T>& rhs, const Vector2D<T>& lhs){
			return Vector2D(rhs.x_comp - lhs.x_comp, rhs.y_comp - lhs.y_comp);
		}

		// allow multiplication by a scaler value 
		friend Vector2D<T> operator * (const Vector2D<T>& theVec, const T& SCALER_VAL){
			return Vector2D(theVec.x_comp * SCALER_VAL, theVec.y_comp * SCALER_VAL);
		}

		// ensure commutative property of scaler multiplication	
		friend Vector2D<T> operator * (const T& SCALER_VAL, const Vector2D<T>& theVec){
			return theVec * SCALER_VAL;	
		}

		// equivilence
		friend bool operator == (const Vector2D<T>& rhs, const Vector2D<T>& lhs){		
			return (rhs.x_comp == lhs.x_comp) and (rhs.y_comp == lhs.y_comp);
		}

		// put a 2D vector into an output stream
		friend std::ostream & operator <<  (std::ostream &output, const Vector2D<T>& v2d){
			output << v2d.x_comp << ", " <<  v2d.y_comp; 
			return output;
		}

		// dot product of 2 Vector3D objects	
		static T DotProduct(const Vector2D<T>& rhs, const Vector2D<T>& lhs){
		   return (rhs.x_comp * lhs.x_comp) + (rhs.y_comp * lhs.y_comp);
		}

		// find the angle (radians) between two Vector2D's 
		static T AngleBetween_radians(const Vector2D<T>& rhs, const Vector2D<T>& lhs){
			return acos( Vector2D::DotProduct(rhs, lhs) / ( rhs.Magnitude() * lhs.Magnitude() ) );
		}

		// find the angle (degrees) between two Vector2D's 
		static T AngleBetween_degrees(const Vector2D<T>& rhs, const Vector2D<T>& lhs){
			//return AngleBetween_radians(rhs, lhs) * T(180.0/M_PI);
			return AngleBetween_radians(rhs, lhs) * T(SINGLE_RADIAN_IN_DEGREES);
		}	
};

