#pragma once
#include "Vector2D.hpp"

/*	William Doyle
 *	class Vector3D
 *	December 28th 2020
 *	A vector with an 'x' component, a 'y' component, and a 'z' component
 *	Extends the Vector2D class
 * */
template <class T>
class Vector3D : public Vector2D<T> {
	public:
		T z_comp;

		// construct from 3 doubles 
		Vector3D(const T& x_comp, const T& y_comp, const T& z_comp): Vector2D<T>  (x_comp, y_comp) {
			this->z_comp = z_comp;
		}

		// construct from Vector2D and a double
		Vector3D(const Vector2D<T>& _base, const T& z_comp = 0): Vector2D<T> (_base.x_comp, _base.y_comp){
			this->z_comp = z_comp;
		}

		// override destructor	
		virtual ~Vector3D() override {  }

		// 3D vector scaler multiplication	
		friend Vector3D operator * (Vector3D<T> theVec , const T& SCALER_VAL){
		//friend Vector3D operator * (Vector3D<T>& theVec , const T& SCALER_VAL){ // theVec should not be a reference. Its not safe but will compile.
			return	Vector3D(*static_cast<Vector2D<T> *>(&theVec) * SCALER_VAL, theVec.z_comp * SCALER_VAL);
		}

		// ensure commutative property of scaler multiplication	
		friend Vector3D operator * (const T& SCALER_VAL, const Vector3D<T>& theVec  ){
			return theVec * SCALER_VAL;	
		}

		// addition	
		friend Vector3D operator + (const Vector3D<T>& rhs , const Vector3D<T>& lhs){
			return Vector3D(rhs.x_comp + lhs.x_comp, rhs.y_comp + lhs.y_comp, rhs.z_comp + lhs.z_comp);
		}		

		// if lhs is only 2D (cast to 3D)	
		friend Vector3D operator + (const Vector3D<T>& rhs, Vector2D<T> lhs){
			return rhs + *(static_cast<Vector3D<T>*>(&lhs));
		} 
		
		// if rhs is only 2D (cast to 3D)	
		friend Vector3D operator + (Vector2D<T> rhs, const Vector3D<T>& lhs){
			return *(static_cast<Vector3D<T> *>(&rhs)) + lhs;
		}

		// subtraction
		friend Vector3D operator - (const Vector3D<T>& rhs, const Vector3D<T>& lhs){
			return Vector3D(rhs.x_comp - lhs.x_comp, rhs.y_comp - lhs.y_comp, rhs.z_comp - lhs.z_comp);
		}	

		// if lhs is only 2D (cast to 3D)	
		friend Vector3D operator - (const Vector3D<T>& rhs, Vector2D<T>& lhs){
			return rhs - *(static_cast<Vector3D<T>*>(&lhs));
		}

		// if rhs is only 2D (cast to 3D)	
		friend Vector3D operator - (Vector2D<T>rhs,  const Vector3D<T>& lhs){
			return *(static_cast<Vector3D<T> *>(&rhs)) - lhs;
		}		

		// equivilence
		// can I rework this to use const references? That should be faster (run time)
		friend bool operator == (Vector3D<T> rhs, Vector3D<T> lhs){
			
			return (*static_cast<Vector2D<T> *>(&rhs) == *static_cast<Vector2D<T> *>(&lhs)) and (rhs.z_comp == lhs.z_comp);
		}

		// put Vector3D into an output stream	
		friend std::ostream & operator <<  (std::ostream &output, Vector3D<T> v3d){
			output << *(static_cast<Vector2D<T>*> (&v3d)) << ", " << v3d.z_comp;
			return output;
		}
	
		// calculate magnitude	
		virtual T Magnitude() const override {
			return sqrt( pow(this->x_comp, 2) + pow(this->y_comp, 2) + pow(this->z_comp, 2) );
		}

		// cross product of 2 3D vectors
		static Vector3D CrossProduct(const Vector3D<T>& rhs, const Vector3D<T>& lhs){
			return Vector3D((rhs.y_comp*lhs.z_comp)-(rhs.z_comp*lhs.y_comp), (rhs.z_comp*lhs.x_comp)-(rhs.x_comp*lhs.z_comp), (rhs.x_comp*lhs.y_comp)-(rhs.y_comp*lhs.x_comp));
		}

		// dot product of 2 Vector3D objects	
		static T DotProduct(Vector3D<T> rhs, Vector3D<T> lhs){
			// cast down to Vector2D and add the 3rd component
			// this may seem silly but if we had a class Vector50D, Vector49D, Vector48D, ...
			// each time we would only need to cast one dimension down and add the next component
			// this saves us from having to repeat ourselves to much when the number of dimensions gets big
			return Vector2D<T>::DotProduct(*static_cast<Vector2D<T>*>(&rhs), *static_cast<Vector2D<T> *>(&lhs)) + (rhs.z_comp * lhs.z_comp);
		}

		// find the angle (radians) between two Vector3D's 
		static T AngleBetween_radians(Vector3D<T> rhs, Vector3D<T> lhs){
			return acos( Vector3D::DotProduct(rhs, lhs) / ( rhs.Magnitude() * lhs.Magnitude() ) );
		}		
		
		// find the angle (degrees) between two Vector3D's 
		static T AngleBetween_degrees(Vector3D<T> rhs, Vector3D<T> lhs){
			//return Vector3D<T> ::AngleBetween_radians(rhs, lhs) * T(180.0/M_PI); // why did I do it like this `Vector3D<T>::`?
			//return Vector3D::AngleBetween_radians(rhs, lhs) * T(180.0/M_PI);
			return Vector3D::AngleBetween_radians(rhs, lhs) * T(SINGLE_RADIAN_IN_DEGREES);
		}
};


