#include <iostream>
#include <ostream>
#include <cmath>
#include <cassert>
#include "Vector2D.hpp"
#include "Vector3D.hpp"
/*
 *	William Doyle
 *	December 28th 2020
 *
 *	Notable resourses:
 *	https://www.mathsisfun.com/algebra/vectors-cross-product.html
 *	https://mathinsight.org/definition/magnitude_vector
 *	https://www.mathsisfun.com/algebra/vectors-dot-product.html
 *	https://www.omnicalculator.com/math/angle-between-two-vectors
 *	
 *	TO IMPLEMENT:
 *		Vector addition
 *		Vector subtraction
 *		scaler multiplication
 *		dot product
 *		cross product
 *		equivelence operator	
 *	
 * */


/*	William Doyle
 *	class Vector2D
 *	December 28th 2020
 *	A vector with an 'x' component and a 'y' component
 *
 * */
/*

class Vector2D {
	public:
		double x_comp, y_comp;
		
		// construct a vector (2D) from two doubles
		Vector2D(double x_comp, double y_comp){
			this->x_comp = x_comp;
			this->y_comp = y_comp;	
		}

		// destructor	
		virtual ~Vector2D() {
			std::cout << "2d destructor called\n";
		}

		// addition
		friend Vector2D operator + (Vector2D rhs, Vector2D lhs) {
			return Vector2D(rhs.x_comp + lhs.x_comp, rhs.y_comp + lhs.y_comp);
		}

		// subtraction
		friend Vector2D operator - (Vector2D rhs, Vector2D lhs) {
			return Vector2D(rhs.x_comp - lhs.x_comp, rhs.y_comp - lhs.y_comp);
		}

		// equivilence
		friend bool operator == (Vector2D rhs, Vector2D lhs){
			return (rhs.x_comp == lhs.x_comp) and (rhs.y_comp == lhs.y_comp);
		}

		// allow multiplication by a scaler value 
		friend Vector2D operator * (Vector2D theVec, double SCALER_VAL){
			return Vector2D(theVec.x_comp * SCALER_VAL, theVec.y_comp * SCALER_VAL);
		}
	
		// ensure commutative property of scaler multiplication	
		friend Vector2D operator * (double SCALER_VAL, Vector2D theVec ){
			return theVec * SCALER_VAL;	
		}

		// put a 2D vector into an output stream
		friend std::ostream & operator << (std::ostream &output, Vector2D v2d) {
			output << v2d.x_comp << ", " <<  v2d.y_comp; 
			return output;
		}

		// get the magnitude of this vector (in 2D this is the Pythagorean theorem)
		virtual double Magnitude() {
			return sqrt( pow(this->x_comp, 2) + pow(this->y_comp, 2) );
		}

		// dot product of 2 Vector3D objects	
		static double DotProduct(Vector2D rhs, Vector2D lhs) {
			return (rhs.x_comp * lhs.x_comp) + (rhs.y_comp * lhs.y_comp);
		}

		// find the angle (radians) between two Vector2D's 
		static double AngleBetween_radians(Vector2D rhs, Vector2D lhs) {
			return acos( Vector2D::DotProduct(rhs, lhs) / ( rhs.Magnitude() * lhs.Magnitude() ) );
		}
		
		// find the angle (degrees) between two Vector2D's 
		static double AngleBetween_degrees(Vector2D rhs, Vector2D lhs) {
			return AngleBetween_radians(rhs, lhs) * (180.0/M_PI);
		}

};
*/
/*	William Doyle
 *	class Vector3D
 *	December 28th 2020
 *	A vector with an 'x' component, a 'y' component, and a 'z' component
 *	Extends the Vector2D class
 * */
/*
class Vector3D : public Vector2D {
	public:
		double z_comp;

		// construct from 3 doubles 
		Vector3D(double x_comp, double y_comp, double z_comp): Vector2D (x_comp, y_comp) {
			this->z_comp = z_comp;
		}

		// construct from Vector2D and a double
		Vector3D(Vector2D _base, double z_comp = 0): Vector2D(_base.x_comp, _base.y_comp){
			this->z_comp = z_comp;
		}
	
		// override destructor	
		virtual ~Vector3D() override {
		}
		
		// 3D vector scaler multiplication	
		friend Vector3D operator * (Vector3D theVec, double SCALER_VAL){
			return	Vector3D(*static_cast<Vector2D*>(&theVec) * SCALER_VAL, theVec.z_comp * SCALER_VAL);
		}

		// ensure commutative property of scaler multiplication	
		friend Vector3D operator * (double SCALER_VAL, Vector3D theVec ){
			return theVec * SCALER_VAL;	
		}

		// addition	
		friend Vector3D operator + (Vector3D rhs,  Vector3D lhs) {
			return Vector3D(rhs.x_comp + lhs.x_comp, rhs.y_comp + lhs.y_comp, rhs.z_comp + lhs.z_comp);
		}
		
		// if lhs is only 2D (cast to 3D)	
		friend Vector3D operator + (Vector3D rhs,  Vector2D lhs) {
			return rhs + *(static_cast<Vector3D*>(&lhs));
		}
		
		// if rhs is only 2D (cast to 3D)	
		friend Vector3D operator + (Vector2D rhs,  Vector3D lhs) {
			return *(static_cast<Vector3D*>(&rhs)) + lhs;
		}

		// subtraction
		friend Vector3D operator - (Vector3D rhs,  Vector3D lhs) {
			return Vector3D(rhs.x_comp - lhs.x_comp, rhs.y_comp - lhs.y_comp, rhs.z_comp - lhs.z_comp);
		}
		
		// if lhs is only 2D (cast to 3D)	
		friend Vector3D operator - (Vector3D rhs,  Vector2D lhs) {
			return rhs - *(static_cast<Vector3D*>(&lhs));
		}

		// if rhs is only 2D (cast to 3D)	
		friend Vector3D operator - (Vector2D rhs,  Vector3D lhs) {
			return *(static_cast<Vector3D*>(&rhs)) - lhs;
		}
		
		// equivilence
		friend bool operator == (Vector3D rhs, Vector3D lhs){
			return (*static_cast<Vector2D*>(&rhs) == *static_cast<Vector2D*>(&lhs)) and (rhs.z_comp == lhs.z_comp);
		}
		// put Vector3D into an output stream	
		friend std::ostream & operator << (std::ostream &output, Vector3D v3d) {
			output << *(static_cast<Vector2D*> (&v3d)) << ", " << v3d.z_comp;
			return output;
		}
	
		// calculate magnitude	
		virtual double Magnitude() override {
			return sqrt( pow(this->x_comp, 2) + pow(this->y_comp, 2) + pow(this->z_comp, 2) );
		}
		
		// cross product of 2 3D vectors
		static Vector3D CrossProduct(Vector3D rhs, Vector3D lhs){
			return Vector3D((rhs.y_comp*lhs.z_comp)-(rhs.z_comp*lhs.y_comp), (rhs.z_comp*lhs.x_comp)-(rhs.x_comp*lhs.z_comp), (rhs.x_comp*lhs.y_comp)-(rhs.y_comp*lhs.x_comp));
		}

		// dot product of 2 Vector3D objects	
		static double DotProduct(Vector3D rhs, Vector3D lhs) {
			// cast down to Vector2D and add the 3rd component
			// this may seem silly but if we had a class Vector50D, Vector49D, Vector48D, ...
			// each time we would only need to cast one dimension down and add the next component
			// this saves us from having to repeat ourselves to much when the number of dimensions gets big
			return Vector2D::DotProduct(*static_cast<Vector2D*>(&rhs), *static_cast<Vector2D*>(&lhs)) + (rhs.z_comp * lhs.z_comp);
		}

		// find the angle (radians) between two Vector2D's 
		static double AngleBetween_radians(Vector3D rhs, Vector3D lhs) {
			return acos( Vector3D::DotProduct(rhs, lhs) / ( rhs.Magnitude() * lhs.Magnitude() ) );
		}
		
		// find the angle (degrees) between two Vector2D's 
		static double AngleBetween_degrees(Vector3D rhs, Vector3D lhs) {
			return Vector3D::AngleBetween_radians(rhs, lhs) * (180.0/M_PI);
		}

};
*/
int main () {
	
	Vector2D* v2d = new Vector2D(3.7, 9.4);
	std::cout << *v2d << "\n";
	std::cout << "Magnitude is "<< v2d->Magnitude() << "\n";

	Vector2D* second2dv = new Vector2D(1.1, 2.2);
	std::cout << "addition (2D) " << *v2d + *second2dv << "\n\n";
	std::cout << "subtraction (2D) " << *v2d - *second2dv << "\n\n";
	
	Vector3D* v3d = new Vector3D(-1.2, 2.3, 7.9);
	Vector3D* second3dv = new Vector3D(1.2, 2.3, 7.1);

	std::cout << *v3d << "\n";
	std::cout << "Magnitude is "<< v3d->Magnitude() << "\n";
	std::cout << "addition (3D) " << *v3d + *second3dv << "\n\n";
	std::cout << "subtraction (3D) " << *v3d - *second3dv << "\n\n";

	std::cout << "addition (3D + 2D) " << *v3d + *second2dv << "\n"; 
	std::cout << "addition (2D + 3D) " <<  *second2dv + *v3d << "\n"; 

	std::cout << "3D vec from 2D vec\t" << Vector3D(*v2d) << "\n";
	std::cout << "again but different\t" << Vector3D(*v2d, 7.77) << "\n";
	
	Vector3D* A = new Vector3D(2,3,4);
	Vector3D* B = new Vector3D(5,6,7);

	std::cout << "Cross product of A and B is " << Vector3D::CrossProduct(*A, *B) << "\n";
	std::cout << "Dot product of A and B is " << Vector3D::DotProduct(*A, *B) << "\n\n";

	// test dot product with numbers from mathsisfun.com (see file level comment) because the expected result for these values is known (it's 122)
	Vector3D* C = new Vector3D(4,8,10);
	Vector3D* D = new Vector3D(9,2,7);
	std::cout << "Dot product of C and D is " << Vector3D::DotProduct(*C, *D) << "\n";

	Vector2D* E = new Vector2D(0.0, 1.0);
	Vector2D* F = new Vector2D(0.1, 0.1);

	std::cout << "Angle between E and F in radians is " <<  Vector2D::AngleBetween_radians(*E, *F) << "\n";
	std::cout << "In degrees this is " << Vector2D::AngleBetween_degrees(*E, *F) << "\n\n";
	
	Vector2D* G = new Vector3D(0.0, 1.0, 0.3);
	Vector2D* H = new Vector3D(0.1, 0.1, 1.1);

	std::cout << "Angle between E and F in radians is " <<  Vector3D::AngleBetween_radians(*G, *H) << "\n";
	std::cout << "In degrees this is " << Vector3D::AngleBetween_degrees(*G, *H) << "\n";

	Vector2D* I = new Vector2D(2.0, 3.0); 
	std::cout << "Scaler multiplication of Vector2D. Original " << *I << ". Multiplied by 2.0 gives " << *I * 2.0 << ".\n";
	std::cout << "Scaler multiplication of Vector2D. Original " << *I << ". Multiplied by 2.0 gives " << 2.0 * *I  << ".\n";


	assert ( *I * 3.0 == 3.0 * *I);


	std::cout << "Scaler multiplication of Vector3D. Original " << *D << ". Multiplied by 2.0 gives " << *D * 2.0 << ".\n";
	std::cout << "Scaler multiplication of Vector3D. Original " << *D << ". Multiplied by 2.0 gives " <<  2.0  * *D << ".\n";

	assert ( *D * 3.0 == 3.0 * *D);

	delete v2d;
	delete second2dv;
	delete v3d;
	delete second3dv;
   	delete A;	
   	delete B;
   	delete C;	
   	delete D;
   	delete E;	
   	delete F;
   	delete G;	
   	delete H;
   	delete I;

	return 1;
};
