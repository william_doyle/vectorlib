%:include <iostream>
%:include <ostream>
%:include <cmath>
%:include <cassert>
struct v2 <%double X,Y;v2(double X,double Y)<%v2::X = X;v2::Y = Y;	%>virtual??-v2() <%%>;friend v2 operator + (v2 rhs,v2 lhs) <%
return v2(rhs.X + lhs.X,rhs.Y + lhs.Y);%>friend v2 operator - (v2 rhs,v2 lhs) <%return v2(rhs.X - lhs.X,rhs.Y - lhs.Y);%>friend bool operator == (v2 rhs,v2 lhs)<%
return (rhs.X == lhs.X) and (rhs.Y == lhs.Y);%>friend v2 operator * (v2 theVec,double SCALER_VAL)<%return v2(theVec.X * SCALER_VAL,theVec.Y * SCALER_VAL);%>friend v2 operator * (double SCALER_VAL,v2 theVec )<%return theVec * SCALER_VAL;%>friend std::ostream & operator << (std::ostream &output,v2 v2d) <%
output << v2d.X << "," <<  v2d.Y; 
return output;%>virtual double Magnitude() <%return sqrt( pow(v2::X,2) + pow(v2::Y,2) );%>static double DotProduct(v2 rhs,v2 lhs) <%
return (rhs.X * lhs.X) + (rhs.Y * lhs.Y);%>static double AngleBetween_radians(v2 rhs,v2 lhs) <%
return acos( v2::DotProduct(rhs,lhs) / ( rhs.Magnitude() * lhs.Magnitude() ) );%>static double AngleBetween_degrees(v2 rhs,v2 lhs) <%
return AngleBetween_radians(rhs,lhs) * (180.0/M_PI);%>%>;struct v3:public v2 <%
double Z;v3(double X,double Y,double Z): v2 (X,Y) <%
v3::Z = Z;%>v3(v2 _base,double Z = 0): v2(_base.X,_base.Y)<%v3::Z = Z;%>virtual ??-v3() override <%%>;friend v3 operator * (v3 theVec,double SCALER_VAL)<%
return	v3(*static_cast<v2*>(&theVec) * SCALER_VAL,theVec.Z * SCALER_VAL);%>friend v3 operator * (double SCALER_VAL,v3 theVec )<%
return theVec * SCALER_VAL;%>friend v3 operator + (v3 rhs, v3 lhs) <%
return v3(rhs.X + lhs.X,rhs.Y + lhs.Y,rhs.Z + lhs.Z);%>friend v3 operator + (v3 rhs, v2 lhs) <%
return rhs + *(static_cast<v3*>(&lhs));%>friend v3 operator+(v2 rhs, v3 lhs) <%
return *(static_cast<v3*>(&rhs)) + lhs;%>friend v3 operator-(v3 rhs, v3 lhs) <%
return v3(rhs.X - lhs.X,rhs.Y - lhs.Y,rhs.Z - lhs.Z);%>friend v3 operator - (v3 rhs, v2 lhs) <%
return rhs - *(static_cast<v3*>(&lhs));%>friend v3 operator-(v2 rhs, v3 lhs) <%
return *(static_cast<v3*>(&rhs)) - lhs;%>friend bool operator == (v3 rhs,v3 lhs)<%
return (*static_cast<v2*>(&rhs) == *static_cast<v2*>(&lhs)) and (rhs.Z == lhs.Z);%>friend std::ostream & operator << (std::ostream &output,v3 v3d) <%
output << *(static_cast<v2*> (&v3d)) << "," << v3d.Z;return output;%>virtual double Magnitude() override <%
return sqrt( pow(v3::X,2) + pow(v3::Y,2) + pow(v3::Z,2) );%>static v3 CrossProduct(v3 rhs,v3 lhs)<%
return v3((rhs.Y*lhs.Z)-(rhs.Z*lhs.Y),(rhs.Z*lhs.X)-(rhs.X*lhs.Z),(rhs.X*lhs.Y)-(rhs.Y*lhs.X));%>static double DotProduct(v3 rhs,v3 lhs) <%
return v2::DotProduct(*static_cast<v2*>(&rhs),*static_cast<v2*>(&lhs)) + (rhs.Z * lhs.Z);%>static double AngleBetween_radians(v3 rhs,v3 lhs) <%
return acos( v3::DotProduct(rhs,lhs) / ( rhs.Magnitude() * lhs.Magnitude() ) );%>static double AngleBetween_degrees(v3 rhs,v3 lhs) <%
return v3::AngleBetween_radians(rhs,lhs) * (180.0/M_PI);%>%>;int main () <%
v2* v2d = new v2(3.7,9.4);std::cout << *v2d << "\n";std::cout << "Magnitude is "<< v2d->Magnitude() << "\n";v2* second2dv = new v2(1.1,2.2);std::cout << "addition (2D) " << *v2d + *second2dv << "\n\n";std::cout << "subtraction (2D) " << *v2d - *second2dv << "\n\n";v3* v3d = new v3(-1.2,2.3,7.9);v3* second3dv = new v3(1.2,2.3,7.1);std::cout << *v3d << "\n";std::cout << "Magnitude is "<< v3d->Magnitude() << "\n";std::cout << "addition (3D) " << *v3d + *second3dv << "\n\n";std::cout << "subtraction (3D) " << *v3d - *second3dv << "\n\n";std::cout << "addition (3D + 2D) " << *v3d + *second2dv << "\n"; 
std::cout << "addition (2D + 3D) " <<  *second2dv + *v3d << "\n"; 
std::cout << "3D vec from 2D vec\t" << v3(*v2d) << "\n";std::cout << "again but different\t" << v3(*v2d,7.77) << "\n";v3* A = new v3(2,3,4);v3* B = new v3(5,6,7);std::cout << "Cross product of A and B is " << v3::CrossProduct(*A,*B) << "\n";std::cout << "Dot product of A and B is " << v3::DotProduct(*A,*B) << "\n\n";v3* C = new v3(4,8,10);v3* D = new v3(9,2,7);std::cout << "Dot product of C and D is " << v3::DotProduct(*C,*D) << "\n";v2* E = new v2(0.0,1.0);v2* F = new v2(0.1,0.1);std::cout << "Angle between E and F in radians is " <<  v2::AngleBetween_radians(*E,*F) << "\n";std::cout << "In degrees this is " << v2::AngleBetween_degrees(*E,*F) << "\n\n";v2* G = new v3(0.0,1.0,0.3);v2* H = new v3(0.1,0.1,1.1);std::cout << "Angle between E and F in radians is " <<  v3::AngleBetween_radians(*G,*H) << "\n";std::cout << "In degrees this is " << v3::AngleBetween_degrees(*G,*H) << "\n";v2* I = new v2(2.0,3.0); 
std::cout << "Scaler multiplication of v2. Original " << *I << ". Multiplied by 2.0 gives " << *I * 2.0 << ".\n";std::cout << "Scaler multiplication of v2. Original " << *I << ". Multiplied by 2.0 gives " << 2.0 * *I  << ".\n";assert ( *I * 3.0 == 3.0 * *I);std::cout << "Scaler multiplication of v3. Original " << *D << ". Multiplied by 2.0 gives " << *D * 2.0 << ".\n";std::cout << "Scaler multiplication of v3. Original " << *D << ". Multiplied by 2.0 gives " <<  2.0  * *D << ".\n";assert ( *D * 3.0 == 3.0 * *D);delete v2d,second2dv,v3d,second3dv,A,B,C,D,E,F,G,H,I;%>;
